module.exports = {
  // CSS Modules
  // Docs: https://cli.vuejs.org/guide/css.html#css-modules
  css: {
    loaderOptions: {
      // Passing Options to Pre-Processor Loaders
      // Docs: https://cli.vuejs.org/guide/css.html#passing-options-to-pre-processor-loaders
      sass: {
        prependData: `@import "~@/assets/scss/core/shared";`
      }
    }
  }
}
