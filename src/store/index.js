/* -----------------------------------------------------------------------------
 * Application Store (AS)
 *
 * @description - Application store entry point. Uses scoped module structure.
 * @documentation: Look modules folder for finding documentation
 * -----------------------------------------------------------------------------
 * User module
 * Portfolio module
 * Static module
 * -----------------------------------------------------------------------------
 */

import Vue from 'vue'
import Vuex from 'vuex'
import { loader } from '@/utils/progress'
import moduleUser from './user'
import modulePortfolio from './portfolio'
import moduleStatic from './static'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    moduleUser,
    modulePortfolio,
    moduleStatic
  },
  // Global state --------------------------------------------------------------
  state: {
    loader // nprogress instance
  },
  // Global mutations ----------------------------------------------------------
  mutations: {

  },
  // Global actions ------------------------------------------------------------
  actions: {

    /**
     * Guard
     */
    guard ({ getters }, { to, from, auth, portfolio }) {
      if (!auth) return undefined // public route => just next()
      if (!getters['moduleUser/isAuthorized']) return { name: 'auth' } // Unauthorized user => login page
      if (getters['moduleUser/isAuthorized'] && from.name === null && to.name !== 'processing') return { name: 'processing', params: { to } } // Fix
      if (portfolio && !getters['modulePortfolio/isPortfolioFormed']) return { name: 'home' }
      return undefined // user is authorized => just next()
    },

    /**
     * Loader start
     */
    loaderStart ({ state }) {
      state.loader.start()
    },

    /**
     * Loader done
     */
    loaderDone ({ state }, payload) {
      payload ? state.loader.done(payload) : state.loader.done()
    }

  },
  // Global getters ------------------------------------------------------------
  getters: {

  }

})
