/* -----------------------------------------------------------------------------
 * Portfolio SM Mutations
 *
 * @description - Mutations of Portfolio SM
 * -----------------------------------------------------------------------------
 */

export default {

  // ----------------------------------------------------------- [CONFIGURATION]

  /**
   * Set configuration field
   */
  SET_CONFIGURATION_FIELD (state, { f, v }) {
    state.configuration[f] = v
  },

  // ------------------------------------------------------------------ [FORMED]

  /**
   * Set portfolio formed status
   */
  SET_PORTFOLIO_FORMED_DATA (state, data) {
    state.formed = data
  },

  // ---------------------------------------------------------------------- [UX]

  /**
   * Set ux field
   */
  SET_UX_FIELD (state, { f, v }) {
    state.ux[f] = v
  },

  // ----------------------------------------------------------------- [RESTORE]

  /**
   * Restore module
   */
  RESTORE_MODULE (state) {
    // Configuration
    state.configuration.deposit = 0
    state.configuration.term = 0
    state.configuration.raiting = 11
    state.configuration.ofz = true
    state.configuration.corporate = false
    state.configuration.period = 30
    // Formed
    state.formed.uuid = ''
    state.formed.amount_after_purchase = 0
    state.formed.amount_spent_portfolio = 0
    state.formed.bond_list = null
    state.formed.count_securities = 0
    state.formed.deposit = 0
    state.formed.number_cells = 0
    state.formed.number_types_securities = 0
    state.formed.profitability_portfolio = 0
    // UX
    state.ux.curIdx = 0
    state.ux.curBuid = ''
    state.ux.email = ''
  }

}
