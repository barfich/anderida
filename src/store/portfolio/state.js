/* -----------------------------------------------------------------------------
 * Portfolio SM State
 *
 * @description - State of Portfolio SM
 * -----------------------------------------------------------------------------
 */

export default {

  // 1. Configuration
  configuration: {
    deposit: 0, // сумма инвестиций
    term: 0, // срок инвестирования (лет)
    raiting: 11, // рейтинг
    ofz: true, // тип облигаций (ОФЗ)
    corporate: false, // тип облигаций (корпоративные)
    period: 30 // период выплаты купона
  },

  // 2. Formed
  formed: {
    uuid: '', // уникальный идентификатор сформированного портфеля
    amount_after_purchase: 0,
    amount_spent_portfolio: 0,
    bond_list: null,
    count_securities: 0,
    deposit: 0,
    number_cells: 0,
    number_types_securities: 0,
    profitability_portfolio: 0
  },

  // 3. UX
  ux: {
    curIdx: 0, // idx выбранной облигации
    curBuid: '', // уникальный идентификатор бумаги
    email: ''
  }

}
