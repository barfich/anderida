/* -----------------------------------------------------------------------------
 * Portfolio SM Actions
 *
 * @description - Actions of Portfolio SM
 * -----------------------------------------------------------------------------
 */

import FileSaver from 'file-saver'
import { http } from '@/utils/http'

export default {

  /**
   * Create portfolio
   */
  async createPortfolio ({ getters, commit }) {
    try {
      let { data } = await http({
        url: 'bonds/create-portfolio/',
        method: 'post',
        data: { ...getters.getPortfolioConfiguration },
        token: true, // protected request
        loader: false // do not show nprogress
      })
      if (data.status) {
        commit('SET_PORTFOLIO_FORMED_DATA', data.data)
        // TODO: delete this debug promt
        console.log(`UUID собранного портфеля: ${data.data.uuid}`)
      }
      return {
        status: data.status,
        errors: data.errors
      }
    } catch (e) {
      return {
        status: false,
        errors: {
          global: ['Ошибка сервера']
        }
      }
    }
  },

  /**
   * Change portfolio bond
   */
  async changePortfolioBond ({ getters, commit }, cbuid) {
    try {
      let { data } = await http({
        url: `/bonds/portfolio/${getters.getFormed.uuid}/paper-replacement/`,
        method: 'post',
        data: {
          main_paper: getters.getUX.curBuid,
          replacement_paper: cbuid
        },
        token: true,
        loader: false
      })
      if (data.status) {
        commit('SET_UX_FIELD', { f: 'curBuid', v: '' })
        commit('SET_PORTFOLIO_FORMED_DATA', data.data)
      }
      return {
        status: data.status,
        errors: data.errors
      }
    } catch (e) {
      return {
        status: false,
        errors: {
          global: ['Ошибка сервера']
        }
      }
    }
  },

  /**
   * Fetch PDF
   */
  async fetchPDF ({ state, dispatch }, { svg }) {
    let uuid = state.formed.uuid
    try {
      let { data } = await http({
        url: `bonds/portfolio/${uuid}/create-pdf/`,
        method: 'post',
        data: { svg },
        token: true, // protected request
        loader: false // do not show nprogress
      })
      // Always status true or exception like server error 500
      if (data.status) {
        // Start pdf caller
        let res = await dispatch('getPDF', { uuid, delay: 1000 })
        if (res) {
          return {
            status: true,
            errors: null
          }
        } else {
          return {
            status: false,
            errors: {
              global: ['Ошибка сервера']
            }
          }
        }
      }
    } catch (e) {
      return {
        status: false,
        errors: {
          global: ['Ошибка сервера']
        }
      }
    }
  },

  /**
   * Get PDF
   */
  async getPDF ({ dispatch }, { uuid, delay }) {
    return new Promise(resolve => {
      if (delay > 27000) {
        resolve(false)
      }
      setTimeout(async () => {
        try {
          let data = await http({
            url: `bonds/portfolio/${uuid}/get-pdf/`,
            method: 'get',
            responseType: 'blob',
            headers: {
              Accept: 'application/pdf, application/json'
            },
            token: true, // protected request
            loader: false // do not show nprogress
          })
          if (data.status === 202) {
            resolve(await dispatch('getPDF', { uuid, delay: delay * 3 }))
          } else {
            FileSaver.saveAs(new Blob([data.data], { type: 'application/pdf' }), `${uuid}.pdf`)
            resolve(true)
          }
        } catch (e) {
          resolve(false)
        }
      }, delay)
    })
  },

  /**
   * Send PDF
   */
  async sendPDF ({ state }, { svg }) {
    let uuid = state.formed.uuid
    let email = state.ux.email
    try {
      let { data } = await http({
        url: 'bonds/pdf/',
        method: 'post',
        data: { svg, uuid, email },
        token: true, // protected request
        loader: false // do not show nprogress
      })
      return {
        status: data.status,
        errors: data.errors
      }
    } catch (e) {
      return {
        status: false,
        errors: {
          global: ['Ошибка сервера']
        }
      }
    }
  }
}
