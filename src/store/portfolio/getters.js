/* -----------------------------------------------------------------------------
 * Portfolio SM Getters
 *
 * @description - Getters of Portfolio SM
 * -----------------------------------------------------------------------------
 */

import { $cfg } from '@/config'

export default {

  /**
   * Get portfolio configuration
   */
  getPortfolioConfiguration: state => state.configuration,

  /**
   * Is portfolio formed
   */
  isPortfolioFormed: state => state.formed.uuid.length > 0,

  /**
   * Get portfolio bonds
   */
  getPortfolioBonds (state) {
    if (!state.formed.bond_list) return []
    return Object.keys(state.formed.bond_list).map((b, idx) => {
      return {
        ...state.formed.bond_list[b],
        idx,
        buid: b,
        color: $cfg.colors[idx],
        selected: state.ux.curIdx === idx
      }
    })
  },

  /**
   * Get replace bonds
   */
  getReplaceBonds (state, getters) {
    if (getters.getPortfolioBonds.length === 0) return [] // fallback
    if (state.ux.curBuid.length === 0) return ''
    return Object.keys(state.formed.bond_list[state.ux.curBuid].alternative_papers).map((b, idx) => {
      return {
        ...state.formed.bond_list[state.ux.curBuid].alternative_papers[b],
        idx,
        buid: b,
        color: $cfg.colors[idx]
      }
    })
  },

  /**
   * Get formed
   */
  getFormed: state => state.formed,

  /**
   * Get ux
   */
  getUX: state => state.ux,

  /**
   * Get root URL
   */
  getRootUrl: () => $cfg.api.rootURL

}
