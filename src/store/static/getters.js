/* -----------------------------------------------------------------------------
 * Static SM Getters
 *
 * @description - Getters of Static SM
 * -----------------------------------------------------------------------------
 */

export default {
  /**
   * Get tooltip by
   * @description Returns tooltip content by given group key & id
   * @param {Object} state - Static SM state
   * @param {String} group - Tooltips group
   * @param {String} id - Tooltip id
   * @returns {Object}
   */
  getTooltipBy: state => (group, id) => {
    if (!state.tooltips) {
      return 'Подсказки не получены с сервера'
    }
    if (group in state.tooltips && id in state.tooltips[group].tips) {
      return state.tooltips[group].tips[id].content
    } else {
      return 'Нет подсказки по указанному ключу'
    }
  }
}
