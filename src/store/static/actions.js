/* -----------------------------------------------------------------------------
 * Static SM Actions
 *
 * @description - Actions of Static SM
 * -----------------------------------------------------------------------------
 */

import { $axios } from '@/utils/http'
export default {

  /**
   * Fetch app static
   * @description Fetchs application static data from JSON
   * @param {Object} commit - Static SM mutations
   */
  async fetchAppStatic ({ commit }) {
    try {
      let { data } = await $axios({
        url: 'static/static.json',
        method: 'get'
      })
      commit('SET_STATIC_DATA', { ...data })
      return true
    } catch (e) {
      console.log(e)
    }
  }
}
