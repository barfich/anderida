/* -----------------------------------------------------------------------------
 * Static SM Mutations
 *
 * @description - Mutations of Static SM
 * -----------------------------------------------------------------------------
 */

export default {

  /**
   * Set static data
   * @description Sets given static data to module state
   * @param {Object} state - Static SM state
   * @param {Object} data - Static data from json
   */
  SET_STATIC_DATA (state, data) {
    state.tooltips = data.tooltips // tooltips
  }
}
