/* -----------------------------------------------------------------------------
 * User SM Actions
 *
 * @description - Actions of User SM
 * -----------------------------------------------------------------------------
 */

import ls from 'store'
import { $cfg } from '@/config'
import { http } from '@/utils/http'
import router from '@/router'

export default {

  /**
   * Login
   */
  async login ({ commit }, { login, password }) {
    try {
      let { data } = await http({
        url: 'auth/login/',
        method: 'post',
        data: { login, password },
        token: false, // un-protected request
        loader: false // do not show nprogress
      })
      // Save tokens
      if (data.status) {
        let { access, refresh } = data.data
        ls.set('_adr-refresh', refresh)
        commit('SET_TOKENS', { access, refresh })
      }
      return {
        status: data.status,
        errors: data.errors
      }
    } catch (e) {
      return {
        status: false,
        errors: {
          global: ['Ошибка сервера']
        }
      }
    }
  },

  /**
   * Logout
   */
  async logout ({ state, commit }, req = false) {
    if (req) {
      try {
        await http({
          url: 'auth/logout/',
          method: 'post',
          data: {
            refresh: state.tokens.refresh
          },
          token: true, // protected request
          loader: true // show nprogress
        })
      } catch (e) {
        console.log(e)
      }
    }
    commit('SET_TOKENS', { access: '', refresh: '' })
    commit('RESTORE_MODULE')
    commit('modulePortfolio/RESTORE_MODULE', null, { root: true })
    ls.remove('_adr-refresh')
    router.push({ name: 'auth' })
  },

  /**
   * Fetch user
   */
  async fetchUser ({ commit, dispatch }, { next }) {
    try {
      let { data } = await http({
        url: 'user/',
        method: 'get',
        token: true, // protected request
        loader: true // show nprogress
      })
      if (data.status) {
        commit('SET_USER_DATA', { ...data.data, fetched: true })
        next()
      } else {
        dispatch('logout')
      }
    } catch (e) {
      console.log(e)
    }
  },

  // ------------------------------------------------------------------ [TOKENS]

  /**
   * Get access token
   */
  async getAccessToken ({ state, commit, dispatch }) {
    if (state.tokens.refresh) {
      if (await dispatch('validateAccessToken')) return state.tokens.access
      if (state.tokens.update === null) {
        commit('SET_TOKEN_UPDATE', dispatch('updateTokens')
          .then(res => {
            commit('SET_TOKEN_UPDATE', null)
            return res
          }))
      }
      return state.tokens.update
    } else {
      throw Error('NO_TOKEN') // throw error => logout
    }
  },

  /**
   * Validate access token
   */
  validateAccessToken ({ state }) {
    let token = state.tokens.access
    if (!token) return false // no token => invalid
    try {
      return ((Date.now() / 1000 | 0) + $cfg.tokens.updateTime) < JSON.parse(atob(token.split('.')[1])).exp
    } catch (e) {
      return false // If some troubles with parsing or decoding token
    }
  },

  /**
   * Update tokens
   */
  async updateTokens ({ state, commit, dispatch }) {
    try {
      const { data } = await http({
        url: 'auth/token/access/',
        method: 'post',
        token: false, // un-protected request
        loader: false, // do not show nprogress
        data: {
          refresh: state.tokens.refresh
        }
      })
      if (data.status) {
        let { access, refresh } = data.data
        ls.set('_adr-refresh', refresh)
        commit('SET_TOKENS', { access, refresh })
        return state.tokens.access
      } else {
        dispatch('logout')
      }
    } catch (e) {
      dispatch('logout')
    }
  }

}
