/* -----------------------------------------------------------------------------
 * User SM Getters
 *
 * @description - Getters of User SM
 * -----------------------------------------------------------------------------
 */

export default {

  // -------------------------------------------------------------------- [AUTH]
  /**
   * Is authorized
   */
  isAuthorized (state) {
    return state.tokens.refresh.length > 0
  },

  /**
   * Is fetched data
   */
  isFetchedData (state) {
    return state.profile.fetched
  },

  // ----------------------------------------------------------------- [PROFILE]

  /**
   * User toolbar name
   */
  userToolbarName (state) {
    return `${state.profile.name} ${state.profile.surname}`
  }

}
