/* -----------------------------------------------------------------------------
 * User SM State
 *
 * @description - State of User SM
 * -----------------------------------------------------------------------------
 */

import ls from 'store'

export default {

  // 1. Tokens
  tokens: {
    access: '', // access token string or ''
    refresh: ls.get('_adr-refresh', ''), // refresh token string or ''
    update: null // token update Promise
  },

  // 2. Profile
  profile: {
    id: '', // user ID
    surname: '', // фамилия
    name: '', // имя
    patronymic: '', // отчество
    email: '', // адрес электронной почты
    fetched: false // is user data was fetched
  }

}
