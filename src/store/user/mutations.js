/* -----------------------------------------------------------------------------
 * User SM Mutations
 *
 * @description - Mutations of User SM
 * -----------------------------------------------------------------------------
 */

export default {

  // ------------------------------------------------------------------ [TOKENS]

  /**
   * Set tokens
   */
  SET_TOKENS (state, { access, refresh }) {
    state.tokens.access = access
    state.tokens.refresh = refresh
  },

  /**
   * Set token update
   */
  SET_TOKEN_UPDATE (state, update) {
    state.tokens.update = update // promise
  },

  // ----------------------------------------------------------------- [PROFILE]

  /**
   * Set user data
   */
  SET_USER_DATA (state, data) {
    state.profile = data
  },

  // ----------------------------------------------------------------- [RESTORE]

  /**
   * Restore module
   */
  RESTORE_MODULE (state) {
    // User data
    state.profile.id = ''
    state.profile.surname = ''
    state.profile.name = ''
    state.profile.patronymic = ''
    state.profile.email = ''
    state.profile.fetched = false
  }

}
