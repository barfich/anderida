/* -----------------------------------------------------------------------------
 * User Store Module (User SM)
 *
 * @description Module for application User logic
 * -----------------------------------------------------------------------------
 */

import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'

export default {
  namespaced: true, // Use module namespace feature
  state,
  getters,
  mutations,
  actions
}
