import store from '@/store'
import Auth from '@/views/Auth.vue'
import Configuration from '@/views/Configuration.vue'
import Portfolio from '@/views/Portfolio'
import Replace from '@/views/Replace'
import Processing from '@/views/Processing'

export const routes = [
  {
    path: '/',
    name: 'home',
    component: Configuration,
    meta: {
      requiresAuth: true,
      requiresPortfolio: false
    }
  },
  {
    path: '/portfolio',
    name: 'portfolio',
    component: Portfolio,
    meta: {
      requiresAuth: true,
      requiresPortfolio: true
    }
  },
  {
    path: '/replace',
    name: 'replace',
    component: Replace,
    meta: {
      requiresAuth: true,
      requiresPortfolio: true
    }
  },
  {
    path: '/auth',
    name: 'auth',
    component: Auth,
    meta: {
      requiresAuth: false,
      requiresPortfolio: false
    },
    beforeEnter: (to, from, next) => {
      if (store.getters['moduleUser/isAuthorized']) {
        next({ name: 'home' })
      } else {
        next()
      }
    }
  },
  {
    path: '/processing',
    name: 'processing',
    component: Processing,
    meta: {
      requiresAuth: true,
      requiresPortfolio: false
    },
    beforeEnter: (to, from, next) => {
      if ('to' in to.params) {
        next()
      } else {
        next({ name: 'home' })
      }
    }
  }
]
