import Vue from 'vue'
import VueRouter from 'vue-router'
import { routes } from './routes'
import store from '@/store'

Vue.use(VueRouter)

const router = new VueRouter({
  routes
})

// Global router before each [Hook]
router.beforeEach(async (to, from, next) => {
  next(await store.dispatch('guard', {
    to,
    from,
    auth: to.meta.requiresAuth,
    portfolio: to.meta.requiresPortfolio
  }))
})

export default router
