/* -----------------------------------------------------------------------------
 * Vee Validate
 *
 * @description - Application form fields validation
 * @documentation - https://github.com/axios/axios
 * -----------------------------------------------------------------------------
 * 1. Required rule
 * 2. Length rule
 * 3. Min rule
 * 4. Email rule
 * 5. Required num
 * -----------------------------------------------------------------------------
 */

import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import { required, length, min, email } from 'vee-validate/dist/rules'

Vue.component('ValidationProvider', ValidationProvider) // Validation provider
Vue.component('ValidationObserver', ValidationObserver) // Validation observer

// 1. Required rule
extend('required', {
  ...required,
  message: 'Это поле необходимо заполнить'
})

// 2. Length rule
extend('length', length)

// 3. Min rule
extend('min', min)

// 4. Email rule
extend('email', email)

// 5. Required num
extend('requiredNum', {
  validate: v => v > 0,
  message: 'Это поле необходимо заполнить'
})
