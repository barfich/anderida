/* -----------------------------------------------------------------------------
 * Http
 *
 * @description - Application http client configuration
 * @documentation - https://github.com/axios/axios
 * -----------------------------------------------------------------------------
 */

import axios from 'axios'
import { $cfg } from '@/config'
import store from '@/store'

// Fake axios instance & cfg
const $axios = axios.create({
  baseURL: '/'
})
// Http client instance
const http = axios.create({
  baseURL: $cfg.api.baseURL
})

// Request interceptor
http.interceptors.request.use(async cfg => {
  if (!cfg.token) return cfg // => non-protected request
  try {
    cfg.headers.Authorization = `${$cfg.tokens.prefix} ${await store.dispatch('moduleUser/getAccessToken')}`
  } catch (e) {
    store.dispatch('moduleUser/logout')
    return // Just return
  }
  if (cfg.loader) store.dispatch('loaderStart') // Loader start
  return cfg
})

// Responce interceptor
http.interceptors.response.use(res => {
  if (res.config.loader) store.dispatch('loaderDone') // Loader done
  return res
}, e => {
  store.dispatch('loaderDone') // Loader done
})

export { $axios, http }
