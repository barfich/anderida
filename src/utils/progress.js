/* -----------------------------------------------------------------------------
 * Nprogress
 *
 * @description - Nprogress configuration
 * @docs - https://github.com/rstacruz/nprogress/
 * -----------------------------------------------------------------------------
 */

import NProgress from 'nprogress'

export const loader = NProgress.configure({
  showSpinner: false,
  minimum: 0.25,
  speed: 300
})
