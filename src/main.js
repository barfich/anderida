import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/utils/validator'
import AppLayout from '@/layouts/AppLayout'
import UiSpriteIcon from '@/components/UiSpriteIcon'
import UiTooltip from '@/components/UiTooltip'
import UiButton from '@/components/UiButton'
import UiDetails from '@/components/UiDetails'
import UiLoader from '@/components/UiLoader'
import UiSelect from 'vue-select'
import VModal from 'vue-js-modal'

// Global UI components --------------------------------------------------------
Vue.component(AppLayout.name, AppLayout) // App layout
Vue.component(UiSpriteIcon.name, UiSpriteIcon) // Ui SVG sprite icon
Vue.component(UiTooltip.name, UiTooltip) // Ui Tooltip
Vue.component(UiButton.name, UiButton) // Ui Button
Vue.component(UiDetails.name, UiDetails) // Ui Details
Vue.component(UiLoader.name, UiLoader) // Ui Loader
Vue.component('UiSelect', UiSelect) //  Ui Select

Vue.use(VModal, { componentName: 'UiModal' })

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
